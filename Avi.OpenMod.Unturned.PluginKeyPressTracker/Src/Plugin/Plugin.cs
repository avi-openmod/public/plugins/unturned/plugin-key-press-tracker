using System;

using Avi.OpenMod.Unturned.PluginKeyPressTracker.Services;

using Cysharp.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

using OpenMod.API.Plugins;
using OpenMod.Unturned.Plugins;

[assembly: PluginMetadata(
    "Avi.OpenMod.Unturned.PluginKeyPressTracker",
    Author = "aviadmini",
    DisplayName = "Avi Plugin Key Events",
    Website = "https://avi.pw/discord"
)]

namespace Avi.OpenMod.Unturned.PluginKeyPressTracker;

// ReSharper disable once ClassNeverInstantiated.Global
internal class Plugin : OpenModUnturnedPlugin {

    private const uint LONG_PRESS_DELAY_MIN = 150u;
    internal const uint LONG_PRESS_DELAY_DEFAULT = 250u;

    private readonly PluginKeyEventsService _keyEventsService;

    public Plugin(PluginKeyEventsService keyEventsService, IServiceProvider serviceProvider) : base(serviceProvider)
        => _keyEventsService = keyEventsService;

    private IDisposable _configChangeListener = null!;

    protected override UniTask OnLoadAsync() {

        IChangeToken changeToken = Configuration.GetReloadToken();
        _configChangeListener = ChangeToken.OnChange(() => changeToken, OnConfigReload);
        OnConfigReload();

        return UniTask.CompletedTask;
    }

    protected override UniTask OnUnloadAsync() {
        _configChangeListener.Dispose();
        return UniTask.CompletedTask;
    }

    private void OnConfigReload() {

        uint longPressThreshold = Configuration.GetValue<uint>("long_press_delay");
        if (longPressThreshold < LONG_PRESS_DELAY_MIN) {
            Logger.LogError("Long press threshold <{MinValue}ms in config. Defaulting to {DefaultValue}",
                LONG_PRESS_DELAY_MIN, LONG_PRESS_DELAY_DEFAULT);
            longPressThreshold = LONG_PRESS_DELAY_DEFAULT;
        }

        _keyEventsService.LongPressDelay = TimeSpan.FromMilliseconds(longPressThreshold);

    }

}