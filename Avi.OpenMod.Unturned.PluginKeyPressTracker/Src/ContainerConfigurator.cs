using Autofac;

using Avi.OpenMod.Unturned.PluginKeyPressTracker.Services;

using OpenMod.API.Ioc;

namespace Avi.OpenMod.Unturned.PluginKeyPressTracker;

// ReSharper disable once UnusedType.Global
internal class ContainerConfigurator : IContainerConfigurator {

    public void ConfigureContainer(
        IOpenModServiceConfigurationContext openModStartupContext,
        ContainerBuilder containerBuilder
    ) { // install plugin key service
        containerBuilder.RegisterType<PluginKeyEventsService>()
            .AsSelf()
            .As<IPluginKeyEventsService>()
            .SingleInstance();
    }

}