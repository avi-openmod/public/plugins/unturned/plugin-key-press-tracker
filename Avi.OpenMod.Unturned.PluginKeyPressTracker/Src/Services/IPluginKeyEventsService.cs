using System;

using OpenMod.API.Ioc;
using OpenMod.Unturned.Users;

namespace Avi.OpenMod.Unturned.PluginKeyPressTracker.Services;

/// <summary>
/// Service that emits plugin key events and exposes <see cref="LongPressDelay"/>
/// </summary>
[Service]
public interface IPluginKeyEventsService {

    /// <summary>
    /// Long press delay (time a key needs to be pressed to trigger long press)
    /// </summary>
    public TimeSpan LongPressDelay { get; }

    /// <summary>
    /// Delegate for key events
    /// </summary>
    public delegate void KeyEvent(UnturnedUser user, byte index);

    /// <summary>
    /// Delegate for key long press event
    /// </summary>
    public delegate void LongPressKeyEvent(UnturnedUser user, byte index, TimeSpan holdDuration);

    /// <summary>
    /// Event emitted on plugin key down
    /// </summary>
    public event KeyEvent? OnKeyDown;

    /// <summary>
    /// Event emitted on plugin key short press
    /// </summary>
    public event KeyEvent? OnShortPress;

    /// <summary>
    /// Event emitted on plugin key long press
    /// </summary>
    public event LongPressKeyEvent? OnLongPress;

    /// <summary>
    /// Event emitted on plugin key long press confirmed
    /// </summary>
    public event KeyEvent? OnLongPressConfirmed;

}