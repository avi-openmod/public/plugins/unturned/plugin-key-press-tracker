using System;
using System.Linq;

using Microsoft.Extensions.Logging;

using OpenMod.Unturned.Users;

using SDG.Unturned;

namespace Avi.OpenMod.Unturned.PluginKeyPressTracker.Services;

// ReSharper disable once ClassNeverInstantiated.Global
internal class PluginKeyEventsService : IPluginKeyEventsService, IDisposable {

    #region Services and ctor

    private readonly ILogger<PluginKeyEventsService> _logger;
    private readonly IUnturnedUserDirectory _userDirectory;

    public PluginKeyEventsService(
        ILogger<PluginKeyEventsService> logger,
        IUnturnedUserDirectory userDirectory
    ) {

        // services
        _logger = logger;
        _userDirectory = userDirectory;

        // default config
        LongPressDelay = TimeSpan.FromMilliseconds(Plugin.LONG_PRESS_DELAY_DEFAULT);

        // listen to keys
        PlayerInput.onPluginKeyTick += OnPluginKeyTick;

    }

    public void Dispose() {
        PlayerInput.onPluginKeyTick -= OnPluginKeyTick;
    }

    #endregion

    #region Config

    public TimeSpan LongPressDelay { get; internal set; }

    #endregion

    #region Events

    public event IPluginKeyEventsService.KeyEvent? OnKeyDown;

    public event IPluginKeyEventsService.KeyEvent? OnShortPress;

    public event IPluginKeyEventsService.LongPressKeyEvent? OnLongPress;

    public event IPluginKeyEventsService.KeyEvent? OnLongPressConfirmed;

    #endregion

    #region Event handlers

    private class KeyData {

        public DateTime KeyDownAt { get; set; }

        private bool _isKeyDown;

        public bool IsKeyDown {
            get => _isKeyDown;
            set {
                _isKeyDown = value;
                if (!value) {
                    IsLongPress = false;
                }
            }
        }

        public bool IsLongPress { get; set; }

    }

    private readonly KeyData[] _keys = Enumerable.Repeat(0, ControlsSettings.NUM_PLUGIN_KEYS).Select(_ => new KeyData()).ToArray();

    private void OnPluginKeyTick(Player player, uint simulation, byte index, bool isKeyDown) {

        if (index >= _keys.Length) { // invalid key index
            return;
        }

        if (OnKeyDown == null && OnShortPress == null && OnLongPress == null && OnLongPressConfirmed == null) { // no events to emit
            return;
        }

        KeyData data = _keys[index];
        if (data.IsKeyDown == isKeyDown) { // if same state as previous tick

            // check for long press confirmed
            if (isKeyDown && // when pressing
                !data.IsLongPress && // and not emitted yet
                OnLongPressConfirmed != null && // and need to emit event
                data.KeyDownAt != default && // skip when key down time not set
                data.KeyDownAt + LongPressDelay < DateTime.UtcNow // long press delay has already passed
               ) {

                data.IsLongPress = true;

                OnLongPressConfirmed?.Invoke(_userDirectory.GetUser(player), index);

            }

            return;
        }

        if (OnKeyDown == null && OnShortPress == null && OnLongPress == null) { // no events to emit
            return;
        }

        DateTime timestamp = DateTime.UtcNow;

        data.IsKeyDown = isKeyDown; // persist new key state

        UnturnedUser user = _userDirectory.GetUser(player);

        // key is now down
        if (isKeyDown) {

            data.KeyDownAt = timestamp; // persist press start timestamp

            OnKeyDown?.Invoke(user, index);

            return;
        }

        // key is now up

        if (data.KeyDownAt == default) { // skip when key down time not set
            return;
        }

        try {

            if (OnLongPress == null && OnShortPress == null) { // no events to emit
                return;
            }

            if (data.KeyDownAt + LongPressDelay < timestamp) { // is long press
                TimeSpan duration = timestamp - data.KeyDownAt;
                OnLongPress?.Invoke(user, index, duration);
            } else { // is short press
                OnShortPress?.Invoke(user, index);
            }

        } finally {
            data.KeyDownAt = default; // reset timestamp when done
        }

    }

    #endregion

}